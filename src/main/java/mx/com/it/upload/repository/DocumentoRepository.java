package mx.com.it.upload.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.it.upload.model.Documento;

@Repository("documentoRepository")
public interface DocumentoRepository extends JpaRepository<Documento, Integer> {

}
