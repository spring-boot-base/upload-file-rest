package mx.com.it.upload.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.it.upload.model.Documento;
import mx.com.it.upload.repository.DocumentoRepository;
import mx.com.it.upload.service.DocumentoService;

@Service("documentoService")
public class DocumentoServiceImpl implements DocumentoService {

	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Override
	public Documento guardaDocumento(Documento documento) {
		documento.setFechaCarga(new Date());
		return documentoRepository.saveAndFlush(documento);
	}

	@Override
	public Documento consultaDocumento(Integer id) {
		return documentoRepository.findOne(id);
	}

	@Override
	public Documento actualizaDocumento(Documento documento) {
		Documento documentoBD = documentoRepository.findOne(documento.getId());
		documentoBD.setDocumento(documento.getDocumento());
		documentoBD.setExtensionDocumento(documento.getExtensionDocumento());
		return documentoRepository.saveAndFlush(documentoBD);
	}

}
