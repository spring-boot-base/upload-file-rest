package mx.com.it.upload.service;

import mx.com.it.upload.model.Documento;

public interface DocumentoService {
	
	public Documento guardaDocumento(Documento documento);
	
	public Documento actualizaDocumento(Documento documento);
	
	public Documento consultaDocumento(Integer id);
	
}
