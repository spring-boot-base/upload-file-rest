package mx.com.it.upload.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table(name="documento", catalog="test")
@NamedQuery(name="Documento.findAll", query="SELECT d FROM Documento d")
public class Documento {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name="tipo_documento", nullable = false, length = 50)
	private String tipoDocumento;
	
	@Temporal(TemporalType.DATE)
	@Column(name="fecha_carga", nullable = false)
	private Date fechaCarga;
	
	@Column(name="observaciones", nullable = true, length = 250)
	private String observaciones;
	
	@Lob
	@Column(name="documento", nullable = true)
	private byte[] documento;
	
	@Column(name="extension_documento", nullable = true, length = 50)
	private String extensionDocumento;
	
}
