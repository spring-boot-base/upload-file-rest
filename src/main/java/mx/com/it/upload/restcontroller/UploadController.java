package mx.com.it.upload.restcontroller;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import mx.com.it.upload.model.Documento;
import mx.com.it.upload.service.DocumentoService;

@RestController
@RequestMapping("/documento")
public class UploadController {
	private static final Logger log = Logger.getLogger(UploadController.class.getName());

	private final Tika defaultTika = new Tika();

	@Autowired
	private DocumentoService documentoService;

	@RequestMapping(value = "/guardaDatos", method = RequestMethod.POST)
	public ResponseEntity<?> saveFileUpload(@RequestBody Documento documento) {
		try {
			log.info("Guardando datos del documento!!!");
			documento.setId(null);
			return new ResponseEntity<>(documentoService.guardaDocumento(documento), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("{\"resultado\":\"¡La carga fue fallida ocurrio un error!\"}",
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/carga/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateFileUpload(@PathVariable Integer id, @RequestParam("file") MultipartFile file) {
		Documento documento = new Documento();
		if (!file.isEmpty()) {
			try {
				log.info("Nombre: " + file.getName());
				log.info("Datos file: " + file.getBytes());
				documento.setDocumento(file.getBytes());
				documento.setExtensionDocumento(defaultTika.detect(documento.getDocumento()));
				log.info("Extension mime: " + documento.getExtensionDocumento());
			} catch (Exception e) {
				return new ResponseEntity<>("{\"resultado\":\"¡La carga fue fallida ocurrio un error!\"}",
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		if (id != null && id.intValue() > 0) {
			documento.setId(id);
			documentoService.actualizaDocumento(documento);
			return new ResponseEntity<>("{\"resultado\":\"¡¡La carga del documento fue exitosa!\"}", HttpStatus.OK);
		} else {
			return new ResponseEntity<>(
					"{\"resultado\":\"¡La carga no se realizo ya que el id no contiene un valor valido!\"}",
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/descarga/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> fileDownload(@PathVariable Integer id, HttpServletResponse response) {
		Documento documento = documentoService.consultaDocumento(id);
		String[] extension = documento.getExtensionDocumento().split("/");
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=documento_TipoDocumento_" + documento.getId() + "." + extension[1])
				.header(HttpHeaders.CONTENT_TYPE, documento.getExtensionDocumento())
				.body(documento.getDocumento());
	}

}
